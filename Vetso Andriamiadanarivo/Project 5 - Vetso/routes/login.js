var express = require("express");
var request = require("request");
var bodyParser = require("body-parser");
var session = require("express-session");

var router = express.Router();

router.get("/", function (req, res) {
  var message = "";
  res.render("login", { message: message });
});

router.post("/", function (req, res) {
  var message = "";
  var User = require("../controllers/user");
  if (req.method == "POST") {
    var { username, password } = req.body;
    User.getUser(username, function (results) {
      if (results.length) {
        console.log(results[0]["user_id"]);
        req.session.userId = results[0]["user_id"];
        res.status(200);
        res.redirect("/");
      } else {
        message = "Erreur de password!!!";
        res.render("login", { message: message });
      }
    });
  } else {
    res.render("login", { message: message });
  }
});

module.exports = router;
