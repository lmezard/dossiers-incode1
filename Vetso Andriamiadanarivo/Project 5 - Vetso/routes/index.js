var express = require("express");
var request = require("request");
var moyenneMovie = require("../controllers/notation");

var router = express.Router();

var url = "https://yts.mx/api/v2/list_movies.json?limit=20";

// permet de recupérer les films via axios
var listMovies = require("../controllers/listMovies");
router.get("/", listMovies.listMovies);

// pour récuperer les infos en JSON de l'API
router.get("/movies", function (req, res) {
  request(url, function (error, response, body) {
    var movies = [];
    movie_json = JSON.parse(body).data.movies;
    movie_json.forEach((element) => {
      var rate = moyenneMovie.getRate(element.id);
      // creation de l objet movie pour pouvoir rajouter l element rate de la BdD
      movie = {
        id_movie: element.id,
        cover_image: element.medium_cover_image,
        title: element.title,
        synopsis: element.synopsis,
        year: element.year,
        rate: rate,
      };
      movies.push(movie);
    });
    console.log("movies", movies);
    res.json(movies);
  });
});

module.exports = router;
