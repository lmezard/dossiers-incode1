var express = require("express");
var movieApi = require("../controllers/movies.api");
var bodyParser = require("body-parser");
var session = require("express-session");

var router = express.Router();
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// pour créer une nouvelle notation par l'user_id
router.post("/", (req, res) => {
  var message = "";
  var Notation = require("../controllers/notation");
  if (req.session.userId != "undefined") {
    // creation de l objet movie_notation ( rate, movie_id du form et user_id de la session)
    var movie_notation = {
      rate: req.body.rate,
      movie_id: req.body.movie_id,
      user_id: req.session.userId,
    };
    console.log(movie_notation);
    Notation.createNotation(movie_notation, function () {
      res.status(200);
    });
    res.redirect("/");
  } else {
    message = "Error : Please login before";
    console.log(message);
    res.render("login", { message: message });
  }
});

// pour afficher le détail du film et pour pouvoir le noter
router.get("/", async function (req, res) {
  var moyenneMovie = require("../controllers/notation");
  var userId = req.session.userId;
  if (userId == null) {
    res.redirect("/login");
    return;
  }

  const { data: movie_json } = await movieApi.getMoviesById(
    req.query["id_movie"]
  );
  moyen = 0;
  // recuperation de la moyenne generale venant de la BdD
  moyenneMovie.getMoyenneNotation(movie_json.data.movie.id, function (average) {
    if (average.length) moyen = average[0]["AVG(rate)"];
    var movie_details = {
      id: movie_json.data.movie.id,
      cover_image: movie_json.data.movie.medium_cover_image,
      title: movie_json.data.movie.title,
      synopsis: movie_json.data.movie.description_intro,
      year: movie_json.data.movie.year,
      rate: moyen,
    };
    res.render("note", { movie_details: movie_details });
  });
});

module.exports = router;
