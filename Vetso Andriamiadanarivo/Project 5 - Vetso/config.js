var mysql = require("mysql");
console.log("Get connection ...");

// connexion à la BdB
var conn = mysql.createConnection({
  database: "project5",
  host: "localhost",
  user: "root",
  password: "password",
});

conn.connect(function (err) {
  if (err) throw err;
  console.log("BDD Connected!");
});

global.mysql = conn;

module.exports.conn = conn;
