var axios = require("axios");

module.exports = {
  getMovies: async () => {
    return await axios.get("https://yts.mx/api/v2/list_movies.json?limit=20");
  },

  getMoviesById: async (id_movie) => {
    return axios.get(
      "https://yts.mx/api/v2/movie_details.json?movie_id=" + id_movie
    );
  },

  getMoviesByGenre: async (genre) => {
    return await axios.get(
      "https://yts.mx/api/v2/list_movies.json?genre=$(genre)&limit=5"
    );
  },
};
