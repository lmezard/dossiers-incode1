var movieApi = require("./movies.api");

module.exports = {
  listMovies: (req, res) => {
    var getMovies = movieApi.getMovies();
    getMovies
      .then((response) => {
        var movie_data = response.data.data.movies;
        res.render("index", { movie_data: movie_data });
      })
      .catch((error) => {
        console.log(error);
      });
  },
};
