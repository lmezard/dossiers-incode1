var { conn } = require("../config.js");

class Notation {
  static getMoyenneNotation(movie_id, callBack) {
    var request =
      "SELECT AVG(rate) FROM notation WHERE movie_id ='" +
      movie_id +
      "' GROUP BY movie_id;";
    conn.query(request, function (err, rows) {
      if (err) {
        console.log("Error db");
      } else {
        callBack(rows);
        return rows;
      }
    });
  }

  static createNotation(req, cb) {
    console.log("Avant creation", req);
    var sql = "INSERT INTO notation SET rate = ?, movie_id = ?, user_id = ?";
    conn.query(sql, [req.rate, req.movie_id, req.user_id], function (
      error,
      results
    ) {
      if (error) throw error;
      console.log("Insert a record!");
    });
  }
}

function getRateGeneral(movie_id, callBack) {
  var request =
    "SELECT AVG(rate) FROM notation WHERE movie_id ='" +
    movie_id +
    "' GROUP BY movie_id;";
  conn.query(request, function (err, rows) {
    if (err) {
      console.log("Error db");
    } else {
      if (rows.length) callBack(rows[0]["AVG(rate)"]);
      return rows;
    }
  });
}

// on renvoie la moyen par rapport à l'id_movie
var result = 0;
function getRate(id_movie) {
  result = 0;
  getRateGeneral(id_movie, (e) => {
    result = e;
    console.log("Rate : ", result);
  });

  return result;
}

module.exports = Notation;
module.exports.getRate = getRate;
