$(function () {
  $(document).ready(function () {
    $.ajax({
      url: "/movies",
      type: "GET",
      dataType: "json",

      success: function (movie_data, status) {
        // affichage dans l index.ejs des movie_data recues de l index.js 
        var result = '<table style="width:100%"><hr/>';
        movie_data.forEach((element) => {
          result +=
            '<tr><td><img src="' +
            element["cover_image"] +
            '" alt="cover"></td><td><div class="desc"><a href="/note?id_movie=' +
            element["id_movie"] +
            '"">' +
            element["title"] +
            '</a></div><div class="desc">' +
            element["id_movie"] +
            '</div><div class="desc">' +
            element["synopsis"] +
            '</div><div class="desc">Year : ' +
            element["year"] +
            '</div><div class="desc">Rating : ' +
            element["rate"] +
            "</div></td></tr>";
        });
        $("#afficherAJAX").html(result + "</table>");
      },

      error: function (result, movie_data, error) {
        console.log("result: ", result);
        console.log("data: ", movie_data);
        console.log("error: ", error);
      },
    });
  });
});
