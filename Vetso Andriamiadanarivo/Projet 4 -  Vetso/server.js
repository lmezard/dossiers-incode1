
const connection = require ('./models/init.js')
const express = require('express')
const app = express()
var bodyParser = require('body-parser')
let bcrypt = require('bcrypt');
const saltRounds = 5

// Session
var session = require('express-session');
app.use(session({
	secret: 'ma session',
	resave: false,
	saveUninitialized: true,
	cookie: { maxAge: 60000 }
  }))


// Moteur de template
app.set('view engine', 'ejs');
app.use('/public', express.static('public'));
// ========================== Middleware ========================
var urlencodedParser = bodyParser.urlencoded({ extended: false })


// ========================== Route =======================

// pour afficher formulaire vers front
app.get('/signup',function (request, response) {
	var message = '';
  	response.render('signup.ejs',{message: message});
  	response.status(200);
});

// pour créer un nouveau user
app.post('/signup',urlencodedParser, (req,res) => {
	var message = '';
	console.log(req.body);	
	let User= require('./models/user')
	var {First_Name, Last_Name, Email, Password, Password2} = req.body;
	if (Password !== Password2){
		message = 'Erreur : le password n est pas confirmé correctement !!!';
		res.render('signup.ejs',{message: message})
		res.status(404);
		console.log("Insert error");
	}
	else {
		User.createUser(req.body, function() {
		})
		res.redirect('/login')
	}
});

//pour afficher login vers front
app.get('/login',function (request, response) {
	var message = '';
	response.render('login.ejs',{message: message});
});

// pour se connecter
app.post('/login',urlencodedParser,function (request, response) {
	var message = '';
	let User= require('./models/user');
	if(request.method == "POST"){
		var {email, password} = request.body;
		User.getUser(email,function (results) {
			if(results.length > 0){
				bcrypt.compare(password, results[0]["Password"], function(err, result) {
				if(result){
					console.log(results[0]["user_ID"]);
					request.session.userId =results[0]["user_ID"]; 
					response.status(200);
					response.redirect('/');
				}
				else {
					message = 'Erreur de password!!!';
					response.render('login.ejs',{message: message});
				}	 
				});
		}
		});
	 } else {
		response.render('login.ejs',{message: message});
	 }
});

//afficher myprofil de l'userId du session et tous les schedules de ce dernier
app.get('/user', (req,res) => {
		//CHeck session
	var userId = req.session.userId;
	if(userId == null){
		res.redirect("/login");
		return;
	}

	let User= require('./models/user')
	User.showUserbyId(userId,function(users) {
		console.log(users)
		res.render('user',{users:users})	
	})
});

// afficher tous les plannings
app.get('/', (req,res) => {
	let Schedules = require('./models/schedules')
	Schedules.allPlannings(function(plannings) {
		//console.log(plannings)
		res.render('index',{plannings:plannings})	
	})
});

// pour se deconnecter d une session
app.get('/logout', function(req,res){
	req.session.destroy(function(err) {
	res.redirect("/login");
	})
});	

// afficher tous les schedules existant de l'user_ID deja connecté
app.get('/schedule',(req,res) => {
	let Schedules = require('./models/schedules');
	var userId = req.session.userId;
	if(userId == null){
	   res.redirect("/login");
	   return;
	}
	Schedules.allSchedulesbyUserId(userId,function(schedules) {
		res.render('schedule',{schedules:schedules})	
	})
});

// pour inserer le nouveau plan dans la base de l'user_ID deja connecté 
app.post('/schedule',urlencodedParser, function(request,response){
	let Schedules = require('./models/schedules');
	var userId = request.session.userId;
	if(userId == null){
		response.redirect("/login");
	   return;
	}
	var req = [ userId, request.body.Day, request.body.Start_Time, request.body.End_Time ];
	Schedules.creatPlanning(req,function () {
	})
	response.redirect('/schedule');
	
});	

// le port du localhost
app.listen(8080, function () {
  	console.log('The app listening on port 8080!')
})

