let mysql = require('mysql');
 
let connection = mysql.createConnection({
  database: 'project4',
  host: "localhost",
  user: "root",
  password: "password"
});
 
connection.connect()

module.exports = connection

// initialisation BDD 
var sql1 ="CREATE TABLE IF NOT EXISTS schedules" +
"(ID_schedules SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,"+
"user_ID SMALLINT(45) UNSIGNED NOT NULL,"+
"Day VARCHAR(15) NOT NULL NOT NULL,"+
"Start_Time TIME NOT NULL,"+
"End_Time TIME NOT NULL,"+
"PRIMARY KEY (ID_schedules))";

var sql2 ="CREATE TABLE IF NOT EXISTS users"+
"(user_ID SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,"+ 
"First_Name VARCHAR(255) NOT NULL,"+
"Last_Name VARCHAR(255) NOT NULL,"+
"Email VARCHAR(45) NOT NULL,"+
"Password VARCHAR(200) NOT NULL,"+
"PRIMARY KEY (user_ID))";