let connection = require ('./init.js')
let bcrypt = require('bcrypt');
const saltRounds = 5;

class User {

    //insertion valeur dans la table 'users'
    static createUser(req, cb) {
        var sql ="INSERT INTO users SET First_Name = ?, Last_Name = ?, Email = ?, Password = ?";
        bcrypt.hash(req.Password, saltRounds, function(err, hashedPass) {
         if(err){
           console.log("Hash error");
         }
         else{
            connection.query(sql, [req.First_Name, req.Last_Name, req.Email, hashedPass], function (error, results) {
            if (error) throw error;
              console.log("Insert a record!");
              cb(results);
            })
         }
      });
    }

   // permet de récuperer toutes les infos de l'user qui se connecte
   static getUser(email, callBack ){
      var request = "Select * From users where Email ='" + email + "'";
      connection.query(request, function(err, rows) {
        if(err) {
          console.log("Error db");
        }else{
           callBack(rows);
           return rows;
        }

      })
    }
        
    //affichage du profil de l'user connecter
    static showUserbyId(userId, cb){   
      var sql1 = "SELECT * FROM users LEFT JOIN schedules ON users.user_ID = schedules.user_ID WHERE users.user_ID = '"+userId+"'";
      connection.query(sql1, function(err,rows) {
        if (err) throw err;
        cb(rows)
      });
    } 

}

module.exports = User

