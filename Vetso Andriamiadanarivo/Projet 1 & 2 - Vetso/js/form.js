document.addEventListener('DOMContentLoaded',function() {
document.forms["myForm"].addEventListener("submit", function(e) {
	    e.preventDefault();
        // Declaration des variables
	    var erreur ='';
        // Recuperation des valeurs du formulaire
	    var nom=document.forms["myForm"].nom.value; 
	    var prenom=document.forms["myForm"].prenom.value; 
	    var tel=document.forms["myForm"].tel.value; 
	    var email=document.forms["myForm"].mail.value; 
	    var message=document.forms["myForm"].mes.value; 	
	    var bol = 'true';

        // Test des valeurs saisies de chaque champs
	    if(nom.length<2){  
 		  erreur="Entrez un nom correct";
		  bol = 'false';
		}

	    if(prenom.length<2){  
 		  erreur="Entrez un prenom correct";
		  bol = 'false';
		}

	    if (isNaN(tel)){
		  erreur="Entrez un chiffre";
		  bol = 'false';
		}

	    if(email.length<5){  
 		  erreur="Un email doit avoir un @ et un . ";
		  bol = 'false';
		}

        // Envoie le message dans le HTML si les tests ci-dessus renvoi 'false'
        document.getElementById("erreur").innerHTML=erreur;

        // Test si le boolean est toujours  'true' et affiche dans le console  puis fais paraitre le popup
        
		if (bol == 'true'){
	    console.log('Nom : ' + nom + "\n",
	    			'Prenom : ' + prenom + "\n", 
	    			'Telephone : ' + tel + "\n",
	    			'Email : ' + prenom + "\n");
	    console.log('Voici le message : ' + message);
        modal.style.display = "block";
	    }
	});

    // Declaration du variable du pop up
    var modal = document.getElementById("myModal");

    // Quand on clique en dehors du pop up, celui ci se ferme
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    // Declaration du constante pour le bouton 'Fermer' du pop up
    const buttonElement = document.getElementById('btnClose');

    // Quand le button 'Fermer' est cliqué le pop up se ferme
    buttonElement.addEventListener('click', function (event) {
       modal.style.display = "none";
    });  
});




