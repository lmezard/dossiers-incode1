const express = require("express");
const router = express.Router();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var Location = require("../models/location");

const locationCtrl = require("../controller/createLocation");

router.get("/", function (req, res) {
  //CHeck session
  var userId = req.session.userId;
  if (userId == null) {
    res.redirect("/login");
    return;
  }
  res.status(200);
  res.render("createLocation");
});

router.post("/", locationCtrl.createLocation);

router.get("/:id", (req, res, next) => {
  Location.findOne({ _id: req.params.id })
    .then((location) => {
      res.status(200);
      res.render("pageDetails", { location: location });
    })
    .catch((error) => res.status(400).json({ error }));
});

router.put("/:id", (req, res, next) => {
  Location.findOne({ _id: req.params.id })
    .then((location) => res.status(200).json(location))
    .catch((error) => res.status(400).json({ error }));
});

router.post("/:id", locationCtrl.addComment);

module.exports = router;
