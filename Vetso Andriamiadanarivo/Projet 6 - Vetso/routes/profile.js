const express = require("express");
const router = express.Router();
const userCtrl = require("../controller/users");
var profile = require("../controller/users");

router.get("/:id", userCtrl.profile);

router.get("/:id/edit", function (req, res) {
  //CHeck session
  var userId = req.session.userId;
  if (userId == null) {
    res.redirect("/login");
    return;
  }
  res.status(200);
  res.render("editProfile");
});

router.put("/:id/edit", userCtrl.editProfile);
router.post("/:id/edit", userCtrl.editProfile);
module.exports = router;
