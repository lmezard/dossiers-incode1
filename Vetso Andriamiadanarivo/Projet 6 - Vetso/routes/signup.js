const express = require("express");
const router = express.Router();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");

var User = require("../models/users");
const userCtrl = require("../controller/users");

router.get("/", function (req, res) {
  var message = "";
  res.status(200);
  res.render("signup", { title: "title" });
});

router.post("/", userCtrl.signup);

module.exports = router;
