var express = require("express");
var router = express.Router();
const Location = require("../models/location");

/* GET home page. */
router.get("/", (req, res, next) => {
  Location.find({ isValid: "true" })
    .then((locations) => {
      console.log("maaaaa", locations);
      res.status(200);
      res.render("index", { locations: locations });
    })
    .catch((error) => res.status(400).json({ error }));
});
module.exports = router;
