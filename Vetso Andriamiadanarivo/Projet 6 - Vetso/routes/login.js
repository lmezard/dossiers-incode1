const express = require("express");
const router = express.Router();
const userCtrl = require("../controller/users");

router.get("/", function (req, res) {
  var message = "";
  res.status(200);
  res.render("login", { title: "title" });
});

router.post("/", userCtrl.login);

module.exports = router;
