const express = require("express");
const router = express.Router();
const Location = require("../models/location");
const userCtrl = require("../controller/admin.js");

var local = " ";
router.get("/", (req, res, next) => {
  //CHeck session
  var userId = req.session.userId;
  var isAdmin = req.session.isAdmin;
  if (userId == null) {
    res.redirect("/login");
    return;
  }
  if (isAdmin == false) {
    alert("Error : you are not a admin");
    res.redirect("/");
    return;
  }
  Location.find({ isValid: "false" }).exec(function (err, locations) {
    if (err == true) {
      res.send("err");
    } else {
    }
    res.render("admin", { locations: locations });
  });
});

router.get("/:id", (req, res, next) => {
  Location.findOne({ _id: req.params.id })
    .then((location) => res.status(200).json(location))
    .catch((error) => res.status(400).json({ error }));
});

router.post("/:id", userCtrl.admin);

module.exports = router;
