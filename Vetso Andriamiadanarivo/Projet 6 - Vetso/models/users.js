var mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

var userSchema = mongoose.Schema({
  username: { type: String },
  email: { type: String },
  password: { type: String },
  isAdmin: { type: Boolean },
});

userSchema.plugin(uniqueValidator);
module.exports = mongoose.model("User", userSchema);
