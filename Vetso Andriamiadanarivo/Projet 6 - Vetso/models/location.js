const uniqueValidator = require("mongoose-unique-validator");

const mongoose = require("mongoose");

const locationSchema = mongoose.Schema({
  name: { type: String, required: true },
  text: { type: String, required: true },
  file: { type: String, required: true },
  like: { type: Number },
  comment: [{  text: { type: String },author: { type: String },idAuthor: { type: String }}],
  isValid: { type: Boolean }});

locationSchema.plugin(uniqueValidator);

module.exports = mongoose.model("Location", locationSchema);
