var mongoose = require("mongoose");

var conn = mongoose
  .connect(
    "mongodb+srv://userAdmin:Admin@cluster0-kijsa.mongodb.net/project6?retryWrites=true&w=majority",
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(() => console.log("Connexion à MongoDB réussie !"))
  .catch(() => console.log("Connexion à MongoDB échouée !"));

global.mongoose = conn;
module.exports.conn = conn;
