const bcrypt = require("bcryptjs");
var mongoose = require("mongoose");
var User = require("../models/users");
var session = require("express-session");

exports.signup = (req, res, next) => {
  bcrypt.hash(req.body.password, 10).then((hash) => {
    var newUser = new User({
      username: req.body.username,
      email: req.body.email,
      password: hash,
      isAdmin: false,
    });
    newUser.save(function (err) {
      if (err == true) {
        console.log("erreur d'écriture");
      } else {
      }
    });
    console.log("enregistrement effectué");
    res.redirect("login");
  });
};

exports.login = (req, res, next) => {
  User.findOne({ email: req.body.email })
    .then((user) => {
      if (!user) {
        return res.status(401).json({ error: "User cannot be found!" });
      }
      bcrypt.compare(req.body.password, user.password).then((valid) => {
        console.log(user);
        if (!valid) {
          return res.status(401).json({ error: "Password incorrect !" });
        }
        req.session.userId = user._id;
        req.session.username = user.username;
        req.session.isAdmin = user.isAdmin;
        console.log("ma szss", req.session.userId);
        res.redirect("/");
      });
    })
    .catch((error) => res.status(500).json({ error }));
};

exports.profile = (req, res, next) => {
  User.findOne({
    _id: req.params.id,
  })
    .then((user) => {
      res.status(200);
      res.render("profile", { user: user });
    })
    .catch((error) => {
      res.status(404).json({
        error: error,
      });
    });
};

exports.editProfile = (req, res, next) => {
  User.update(
    { _id: req.params.id },
    { username: req.body.username, email: req.body.email }
  ).exec(function (err) {
    if (err == true) {
      res.send("err");
    } else {
      res.redirect("/");
    }
  });
};
