var mongoose = require("mongoose");

const Location = require("../models/location");

const fs = require("fs");

exports.createLocation = (req, res, next) => {
  var newLocation = new Location({
    name: req.body.name,
    text: req.body.text,
    file: "../images/" + req.body.file,
    like: "",
    comment: [],
    isValid: false,
  });
  newLocation.save(function (err) {
    if (err == true) {
      console.log("erreur d'écriture");
    } else {
    }
  });
  res.redirect("/");
};

exports.addComment = (req, res, next) => {
  //CHeck session
  var userId = req.session.userId;
  if (userId == null) {
    res.redirect("/login");
    return;
  }
  var location;
  Location.findOne({ _id: req.params.id })
    .then((e) => {
      location = e
      var newComment = {}
      newComment.text = req.body.text;
      newComment.author = req.session.username;
      newComment.idAuthor = req.session.userId;
      location.comment.push(newComment)
      //console.log("avant updateOne location", location);
      Location.update({ _id: req.params.id }, location )
        //.then(() => res.status(200).redirect("/"))
        .then(() => res.status(200).json({ message: "Objet modifié !" }))
        .catch((error) => res.status(400).json({ error }));
    })
    .catch((error) => res.status(400).json({ error }));
};

exports.deleteLocation = (req, res, next) => {
  Location.findOne({ _id: req.params.id })
    .then((location) => {
      const filename = location.imageUrl.split("/images/")[1];
      fs.unlink(`images/${filename}`, () => {
        Location.deleteOne({ _id: req.params.id })
          .then(() => res.status(200).json({ message: "Objet supprimé !" }))
          .catch((error) => res.status(400).json({ error }));
      });
    })
    .catch((error) => res.status(500).json({ error }));
};
