var createError = require("http-errors");
var express = require("express");
var mongoose = require("./config");
var path = require("path");
var multer = require("multer");

var fs = require("fs");

var bodyParser = require("body-parser");
var logger = require("morgan");
var session = require("express-session");

var indexRouter = require("./routes/index");
var signupRouter = require("./routes/signup");
var loginRouter = require("./routes/login");
var profileRouter = require("./routes/profile");
var logoutRouter = require("./routes/logout");
var adminRouter = require("./routes/admin");
var createLocationRouter = require("./routes/createLocation");
var getOneLocationRouter = require("./routes/createLocation");

var app = express();
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

var cookieParser = require("cookie-parser");
app.use(
  session({
    secret: "ma session",
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60000 },
  })
);

app.use(logger("dev"));
app.use(express.json());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, PATCH, OPTIONS"
  );
  next();
});

app.use("/", indexRouter);
app.use("/signup", signupRouter);
app.use("/login", loginRouter);
app.use("/profile", profileRouter);
app.use("/logout", logoutRouter);
app.use("/admin", adminRouter);
app.use("/createLocation", createLocationRouter);
app.use("/getOneLocation", getOneLocationRouter);

//catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
