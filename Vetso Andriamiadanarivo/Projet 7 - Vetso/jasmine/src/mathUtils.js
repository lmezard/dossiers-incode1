function sum(number1, number2) {
    return number1 + number2;
}

function substract(number1, number2) {
    return number1 - number2;
}

function multiply(number1, number2) {
    return number1 * number2;
}

function divide(number1, number2) {
    if (number2 == 0) {
        throw new Error("Divide with 0 is not possible");
    } else {
        return number1 / number2;
    }
}