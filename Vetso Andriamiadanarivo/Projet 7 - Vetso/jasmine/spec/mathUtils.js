    describe("when calc is used to peform basic math operations", function() {
        let num1;
        let num2;

        beforeEach(() => {
            num1 = 3;
            num2 = 5;
        });

        //Spec for sum operation
        it("should be able to calculate sum of 3 and 5", function() {
            expect(sum(num1, num2)).toEqual(8);
        });

        //Spec for substract operation
        it('should be able to substract of 3 and 5', function() {
            const result = substract(num1, num2);
            expect(result).toBe(-2);
        })

        //Spec for multiply operation
        it("should be able to multiply 10 and 40", function() {
            expect(multiply(10, 40)).toEqual(400);
        });

        //Spec for slip operation
        it("should be able to divide 10 and 5", function() {
            expect(divide(10, 5)).toEqual(2);
        });

    });
//before each test integration 