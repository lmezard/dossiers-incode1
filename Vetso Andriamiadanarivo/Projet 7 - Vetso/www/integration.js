// ================== Debut du test integration ================
// pour espace le temps d'execution des actions

function sleep(ms) {
    return new Promise(res => setTimeout(res, ms));
}

var evt = new MouseEvent("click", {
        view: window,
        bubbles: true,
        cancelable: true,
        clientX: 20,
        // whatever properties you want to give it 
    }),
    // soustrait 4 et 9
    ele = document.getElementById("4");
let myIntegrationFunc = async function() {
    await sleep(1000);
    ele.dispatchEvent(evt);
    ele = document.getElementById("-");
    ele.dispatchEvent(evt);
    await sleep(1000);

    ele = document.getElementById("9");
    ele.dispatchEvent(evt);
    await sleep(1000);

    ele = document.getElementById("=");
    ele.dispatchEvent(evt);
    await sleep(1000);

    // le total multiplié par 7
    ele = document.getElementById("*");
    ele.dispatchEvent(evt);
    await sleep(1000);

    ele = document.getElementById("7");
    ele.dispatchEvent(evt);
    await sleep(1000);

    ele = document.getElementById("=");
    ele.dispatchEvent(evt);
    await sleep(1000);

    // le total plus 45
    ele = document.getElementById("+");
    ele.dispatchEvent(evt);
    await sleep(1000);

    ele = document.getElementById("4");
    ele.dispatchEvent(evt);
    await sleep(1000);

    ele = document.getElementById("5");
    ele.dispatchEvent(evt);
    await sleep(1000);

    ele = document.getElementById("=");
    ele.dispatchEvent(evt);
    await sleep(1000);

    // le total divisé 4
    ele = document.getElementById("/");
    ele.dispatchEvent(evt);
    await sleep(1000);

    ele = document.getElementById("4");
    ele.dispatchEvent(evt);
    await sleep(1000);

    ele = document.getElementById("=");
    ele.dispatchEvent(evt);
}
myIntegrationFunc(); 

