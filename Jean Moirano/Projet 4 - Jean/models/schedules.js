'use strict';
module.exports = (sequelize, DataTypes) => {
  const schedules = sequelize.define('schedules', {
    userID: DataTypes.INTEGER,
    day: DataTypes.INTEGER,
    start_at: DataTypes.TIME,
    end_at: DataTypes.TIME
  }, {});
  schedules.associate = function(models) {
    // associations can be defined here
  };
  return schedules;
};