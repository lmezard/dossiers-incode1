var express = require('express');
var bodyParser = require('body-parser');
var ejs = require('ejs');
var path = require('path');
var session = require('express-session');
var Sequelize = require('sequelize');
const bcrypt = require('bcryptjs');
const db = require('../models/index');
const models = require('../models');
const User = db.sequelize.models.users;
const sched = db.sequelize.models.schedules;

//now let's create an accountRoutes
var accountRoutes = express.Router();

//now create routes to show registration page and login page to enter details
accountRoutes.get('/login',function(req,res){
    res.render('account/login');
});
accountRoutes.get('/register',function(req,res){  
    res.render('account/register',{errors: ""});
});
accountRoutes.get('/new_planning',function(req,res){ 
    let email = req.session.email; 
    res.render('home/new_planning',{user_email: email});
});
accountRoutes.get('/users/:id',function(req,res){ 
//let email = req.session.email;
User.findOne({ where: { id:req.params.id}
        //raw:true,
        //attributes : ['firstname','lastname'] 
    })
    .then(function(result){
        //console.log(result);
    res.render('home/users',{result}); 
    //res.render('home/users',{user_email: email});
    })
});
accountRoutes.get('/signout',function(req,res){ 
    console.log('Destroying session');
    req.session.destroy();
    //res.send({ result: 'OK', message: 'Session destroyed';
        res.redirect('/');
    //let email = req.session.email; 
    //res.render('home/signout',{user_email: email});
});
//});


// now create route for registration
accountRoutes.post('/register',function(req,res){
    var matched_users_promise = models.users.findAll({
        where:  Sequelize.or(
                {lastname: req.body.lastname},
                {email: req.body.email}
            )
    });
    matched_users_promise.then(function(users){ 
        if(users.length == 0){
            const passwordHash = bcrypt.hashSync(req.body.password,10);
            models.users.create({
                lastname: req.body.lastname,
                firstname: req.body.firstname,
                email: req.body.email,
                password: passwordHash
            }).then(function(){
                let newSession = req.session;
                newSession.email = req.body.email;
                res.redirect('/');
            });
        }
        else{
            res.render('account/register',{errors: "Username or Email already in user"});
        }
    })
});

// now create route for new planning
accountRoutes.post('/new_planning',function(req,res){
    var matched_users_promise = models.schedules.findAll({
        where:  Sequelize.or(
                {userID: req.body.userID},
                // {email: req.body.email}               
            )
    });
        console.log('1111');

    matched_users_promise.then(function(users){ 
                console.log('2222');

       // if(users.length == 0){
            //const passwordHash = bcrypt.hashSync(req.body.password,10);
                console.log('3333');

            models.schedules.create({
                userID: req.body.userID,
                day: req.body.day,
                start_at: req.body.start_at,
                end_at: req.body.end_at
            }).then(function(){
                console.log('4444');

                let newSession = req.session;
                newSession.email = req.body.email;
                res.redirect('/users');
            });
        //}
        //else{
            //res.render('home/new_planning',{errors: "Username or Email already in user"});
        //}
    })
});


// Route for registered user
accountRoutes.post('/login',function(req,res){
    var matched_users_promise = models.users.findAll({
        where: Sequelize.and(
            {email: req.body.email},
        )
    });
    matched_users_promise.then(function(users){ 
        if(users.length > 0){
            let user = users[0];
            let passwordHash = user.password;
            if(bcrypt.compareSync(req.body.password,passwordHash)){
                req.session.email = req.body.email;
                res.redirect('/');
            }
            else{
                res.redirect('/register');
            }
        }
        else{
            res.redirect('/login');
        }
    });
});

module.exports = {"AccountRoutes" : accountRoutes};