var express = require('express');
var bodyParser = require('body-parser');
var ejs = require('ejs');
const db = require('../models/index');
const models = require('../models');
const User = db.sequelize.models.users;
const sched = db.sequelize.models.schedules;
const chalk = require('chalk');
const cl = console.log;
var path = require('path');
var HomeRoutes = express.Router();
var correct_path = path.join(__dirname+'/../views/home/');

HomeRoutes.get('/',function(req,res){
    let email = req.session.email;
    cl(chalk.red.bold.bgWhite(email+" is connected"));
    User.findOne({
        where:  {email:email}
    })

    //router.get('/', function(req, res) {
    sched.findAll({
        raw:true,
        attributes : ['userID','day','start_at','end_at']
    })
    .then(function(result){
        //res.render('home/index',{user_email: email});
    res.render('home/index',{user_email: email,result});
        //res.send(result);,
    })
});

HomeRoutes.get('/home',function(req,res){
    res.render('home/index');
});

module.exports = {"HomeRoutes" : HomeRoutes};