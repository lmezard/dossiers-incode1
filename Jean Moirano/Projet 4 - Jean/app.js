var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
var bodyParser = require('body-parser');
const db = require('./models/index');
const models = require('./models');
//const User = db.sequelize.models.users;
//onst sched = db.sequelize.models.schedules;
//var indexRouter = require('./routes/index');
//var usersRouter = require('./routes/users');

// models.schedules.findAll().then(function(users){
// console.dir(users)
// // console.log(JSON.stringify(users, null, "  "));
// })


var app = express();
var AccountRoutes = require('./controllers/account_controller');
var HomeRoutes = require('./controllers/home_controller');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret: 'randomstringsessionsecret'}));
//app.use('/', indexRouter);
//app.use('/users', usersRouter);
app.use('/',AccountRoutes.AccountRoutes);

app.use(function(req,res,next){
  if(req.session.email == null || req.session.email.length ==0 ){
      res.redirect('/login'); 
  }
  else{
    next();
  }
});
app.use('/',HomeRoutes.HomeRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.status(404).render('404');
  //next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
