let express = require('express');
let config = require('./config');
let listMovies = require('./controllers/listMovies');
let auth = require('./auth');



let app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.set('view engine', 'ejs');

app.use('/public', express.static(__dirname + '/public'));

app.get('/',/*auth.isAuth,*/ listMovies.listMovies);

app.get('/signup', (req, res)=>{
	res.render('signup');
});

app.get('/login', (req, res)=>{
	res.render('login');
});

app.post('/login', auth.login);

app.post('/signup', auth.signup);

app.get('/logout', auth.logout);
app.post('/logout', auth.logout);


app.get('/films/:id', listMovies.infoFilmId);

app.get('/search', listMovies.search);

app.get('/rate', (req, res)=>{
	MYSQL.query("SELECT film_id, COUNT(film_id) AS nb FROM rate GROUP BY film_id;", (err, result)=>{
		var tab = [];
		var rate = result;
	MYSQL.query("SELECT rate, film_id FROM rate ;", (error, resulta)=>{
			
			var rate2 = resulta;
			
			for(index in rate){
				tab.push({"film_id" : rate[index].film_id, "nb" : rate[index].nb});

			}
			for(index in rate2){
				tab.push({"film_id" : rate2[index].film_id, "rate" : rate2[index].rate});
			}

			console.log(tab)
		
			
			res.status(200).json({tab});
		});
		
		
	});
});

app.listen(8080);