let axios = require('axios');

module.exports = {
	getMovies:async () => {
		return await axios.get('https://yts.ae/api/v2/list_movies.json?limit=20');
	},
	getMoviesDetails : async (filmId) => {
		return await axios.get(`https://yts.mx/api/v2/movie_details.json?movie_id=${filmId}`);
	
	}
}