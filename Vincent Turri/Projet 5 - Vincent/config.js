let mysql = require('mysql');

let conn = mysql.createConnection({
	host : 'localhost',
	user : 'root',
	password : 'Kimono69',
	database : 'projet5',
})

conn.connect(function (err){
	if (err){
		console.log(err);
	}else{
		console.log ("connection OK");
	}
});

global.MYSQL = conn;

let sql = "CREATE TABLE IF NOT EXISTS users " +
        " (id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, " +
        " email VARCHAR(255) NOT NULL, " +
        " password VARCHAR(255) NOT NULL, " +
        " PRIMARY KEY (id) )";

conn.query(sql, function (err, res) {
  if (err) throw err;
  console.log('table users créé');
});

let sql2 = "CREATE TABLE IF NOT EXISTS rate " +
        " (id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, " +
        " rate TINYINT NOT NULL, " +
        " film_id INT NOT NULL, " +
        " user_id SMALLINT NOT NULL, " +
        " PRIMARY KEY (id) )";

conn.query(sql2, function (err, res) {
  if (err) throw err;
  console.log('table rate créé');
});


module.exports = {
  "secret": "pathe"
}