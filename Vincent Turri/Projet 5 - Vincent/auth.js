let bcrypt = require('bcrypt');
const saltRounds = 10;
let secret = require('./config');
let jwt = require('jsonwebtoken');
let cookies = require ('cookie-parser');

function generateToken(id){
	return jwt.sign({id: id}, secret.secret);
}

function login (req, res){
	let {email, psw} = req.body;
	let sql ="SELECT * from users WHERE email = ?;"
	let value = [email];
	MYSQL.query(sql, value, (err, result)=>{
		if(result.length > 0){
			console.log(result)
			for (key in result[0]){
				if (key =='password'){
					bcrypt.compare(psw, result[0][key], (error, resu)=>{
						if(error){
							console(error);
							res.status(200);
							res.render('login', {msg :"le mot de passe et le login ne match pas"});
						}else{
							if(resu){
								console.log(result[0].id)
								let token = generateToken(result[0].id);
								res.cookie('authToken', token );
								res.status(200);
								res.redirect('/');
							}else{
								res.status(200);
								res.render('login', {msg :"le mot de passe et le login ne match pas"});
							}
						}
					})

				}
				
			}
		}
	})
	
}

function logout(req, res){
	res.cookie('authToken', {expires: Date.now()});
	res.clearCookie('authToken');
	res.redirect('/');
}

function isAuth(req, res, next){
	let token = req.Cookies;
	console.log('Cookies: ', req.cookies)
		jwt.verify(token, secret.secret, (err, user) => {
			if(user){
				res.render('index', {status : "déconnexion"});
				next();
			}else{
				res.render('index', {status : "login"});
				next();
			}
		});
}

function signup (req, res){
	let {email, psw, confirmPsw} = req.body;
	let sql2 = "SELECT * FROM users WHERE email=?;";
	MYSQL.query(sql2, [email], function(error, results){
	 	if (results.length == 0){
			if (psw == confirmPsw){
				
				bcrypt.hash(psw, saltRounds, function(err, hash) {
					let sql = "INSERT INTO users (email, password) VALUES (?,?);";
					let value = [email, hash];
					MYSQL.query(sql, value, function(err, result) {
					if (err) throw err;
					 	console.log('user ajouté');
					 	res.status(200);
					 	res.redirect('/login');
					});
				});
			}else{
				res.status(500).json({"msg": "mot de passe pas identique"});
			}
		}else{
			res.status(500).json({"msg": "cette email existe déjà"});
		}
	 })
}

exports.login = login;
exports.signup = signup;
exports.logout = logout;
exports.isAuth = isAuth;