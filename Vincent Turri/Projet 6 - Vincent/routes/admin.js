const express = require('express');
const router = express.Router();

const admin = require('../controller/admin.js');

router.get('/', admin.isAdmin, admin.displayAdmin );
router.post('/accepte', admin.isAdmin, admin.commandAdmin );

module.exports = router;
