var express = require('express');
var multer  = require('multer');
var router = express.Router();
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
     var extent = file.originalname.split('.');
      cb(null, Date.now() + '.' + extent[1]) //Appending .jpg
    }
  })
var upload = multer({ storage : storage })

const places = require('../controller/places.js');
const users = require('../controller/users.js');


router.get('/', users.isAuth, places.displayPlaces);
router.get('/comments', users.isAuth, places.commentPlaces);
router.post('/comments', users.isAuth, places.postCommentPlaces);
router.get('/add', users.isAuth, places.addPlacesForm);
router.post('/add', users.isAuth, upload.single('photo'), places.addPlaces);
router.get('/list', users.isAuth, places.searchPlaces);
router.get('/:id', users.isAuth, places.descrptionPlaces);





module.exports = router;