const express = require('express');
const router = express.Router();

const users = require('../controller/users.js');

router.get('/signup',  users.signupForm);
router.post('/signup',  users.signup);
router.post('/login',  users.login);
router.get('/login', users.loginForm);
router.get('/logout', users.isAuth, users.logout);
router.get('/:id', users.displayProfil);
router.post('/:id', users.modifProfil);



module.exports = router;
