const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var cookieParser = require('cookie-parser')


const path = require('path');

const adminRoutes = require('./routes/admin');
const userRoutes = require('./routes/users');
const placesRoutes = require('./routes/places');

const app = express();

mongoose.connect('mongodb+srv://vturri:Kimono69@cluster0-plcbu.mongodb.net/test?retryWrites=true&w=majority',
  { useNewUrlParser: true,
  	useCreateIndex: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));


app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});


app.use(express.static(__dirname + '/public'));
app.set('views', path.join(__dirname, '/views/'));
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(cookieParser())

app.use('/places', placesRoutes)
app.use('/user', userRoutes)
app.use('/admin', adminRoutes)

module.exports = app;
