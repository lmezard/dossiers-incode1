const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const placesSchema = mongoose.Schema({
  	name: { type: String, required: true, unique: true },
  	description: { type: String, required: true },
 	linkImg: { type: String, required: true },
  	isValid: { type: Boolean, required: true },
 	userId: { type: String, required: true }
});

placesSchema.plugin(uniqueValidator);

module.exports = mongoose.model('PLaces', placesSchema);