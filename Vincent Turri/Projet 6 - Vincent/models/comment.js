const mongoose = require('mongoose'); 

const CommentSchema = mongoose.Schema({
  	content: { type: String, required: true, unique: true },
  	idUsers: { type: String, required: true },
 	idPlaces: { type: String, required: true },
 });


module.exports = mongoose.model('Comment', CommentSchema);