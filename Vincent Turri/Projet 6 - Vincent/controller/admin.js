let secret = require('../config');
let jwt = require('jsonwebtoken');
var cookieParser = require('cookie-parser');
let places = require('../models/places');
let bodyParser = require('body-parser')

function isAdmin (req, res, next){
    let token = req.cookies;
    jwt.verify(token.authToken, secret.secret, (err, users) => { 
        if(users){
            let status = users['isAdmin']
            if(status == true){
                next()
            }else{
                res.redirect('/places/');
                next()
            }
        }  
    });
}



function displayAdmin (req, res){
    places.find({ isValid: false}, function (err, user) {
       res.render('admin',{user});
    })
}

function commandAdmin (req, res){
    myObj = req.body  
        
     
     var idPlaces = Object.keys(myObj)[0];
     var action = Object.values(myObj)[0];
     console.log(action)
     console.log(idPlaces)
     if(action == "supprimer"){
        places.deleteOne({ _id: idPlaces}, function (err, user) {
            res.redirect('/admin/');
            console.log('lieu supprimé')
            res.status(200);
         })
     }if(action == "Ajouter"){
         console.log('ok')
        places.updateOne({ _id: idPlaces }, { isValid: true }, function(err, resu) {
            res.redirect('/admin/');
            console.log('lieu ajouté')
            res.status(200);
         })
     }
     

}
 
exports.isAdmin = isAdmin;
exports.displayAdmin = displayAdmin;
exports.commandAdmin = commandAdmin;