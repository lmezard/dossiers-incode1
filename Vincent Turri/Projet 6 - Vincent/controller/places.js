let places = require('../models/places');
let Comments = require('../models/comment');
let secret = require('../config');
let jwt = require('jsonwebtoken');
var cookieParser = require('cookie-parser');


function commentPlaces (req, res){
    Comments.find( function (err, comment) {
       //res.send("genial") 
       res.status(200).json({comment})
    })
    
} 

function postCommentPlaces (req, res){
    res.status(200).json({"Message" : "test"})
    console.log(req.body)
    console.log('body: ' + JSON.stringify(req.body));
} 

function displayPlaces (req, res){
    places.find({ isValid: true}, function (err, user) {
        let token = req.cookies;
        jwt.verify(token.authToken, secret.secret, (err, users) => { 
            if(users){
                let iduser = users['id']
                res.render('index', {user, iduser});
            }  
           
        });
        
        
    })
    
}

function addPlacesForm (req, res){
    res.render('addPLace');
}

function descrptionPlaces (req, res){
    let idPlace = req.params
    console.log(idPlace['id']);
    places.findOne({ _id: idPlace['id'] }, function (err, user){
        console.log(user)
        res.render('displayPlace', {user});
    })
    
}



function addPlaces (req, res){
 console.log(req.file);
 var placeBody = req.body;
 console.log(placeBody);
 let token = req.cookies;
 jwt.verify(token.authToken, secret.secret, (err, users) => {
    var _p = new places({name: placeBody.name, description : placeBody.description,  linkImg : "../uploads/"+req.file['filename'], isValid : false, userId: users['id'] });
        _p.save(function (err, _user) {
            res.status(200);
            console.log(_user);
            console.log(users['id']);
            console.log("un lieu a été ajouté ! ");
            res.redirect('/places/');
        });
 });

}

function searchPlaces (req, res){
    places.find({ isValid: true}, function (err, user) {
       res.status(200).json({user})
    })
    
} 



   exports.displayPlaces = displayPlaces;
   exports.descrptionPlaces = descrptionPlaces;
   exports.addPlaces = addPlaces;
   exports.addPlacesForm = addPlacesForm;
   exports.searchPlaces = searchPlaces;
   exports.commentPlaces = commentPlaces;
   exports.postCommentPlaces = postCommentPlaces;
