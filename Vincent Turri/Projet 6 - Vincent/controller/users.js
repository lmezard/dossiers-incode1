let users = require('../models/users');
let bodyParser = require('body-parser')
let bcrypt = require('bcrypt');
let secret = require('../config');
let jwt = require('jsonwebtoken');
var cookieParser = require('cookie-parser');



//On génère un token dans une fonction
function generateToken(id, admin){
	return jwt.sign({id: id, isAdmin: admin}, secret.secret);
}


function signup (req, res){
    //On recupérer l'ensemble du formulaire
    userBody = req.body

    //On vérifie que la personne a bien rentré le mot de passe ou l'email
    if (!req.body.email || !req.body.password) {
        res.status(400).json({"text": "Mot de passe ou email non remplit"})
    
    //On verifie si l'utilisateur à les mot de passe identique
    }else if(req.body.password != req.body.cpassword){
        res.status(400).json({"text": "mot de passe non identique"})
    }else{
        let recupEmail = req.body.email;

        //On recherche dans la base de donnée si l'utilisateur existe ou non
        users.findOne({ email: recupEmail }, function (err, user) {
            if(user){
                res.status(400).json({"text": "L'utilisateur existe deja"})
                
            }else{

                //On crypt le mot de passe
                bcrypt.hash(userBody.password, 10, function(err, hash) {

                    //On stock les valeurs exacte dans une variable
                    var _u = new users({name: userBody.name, firstname : userBody.firstname,  email : userBody.email, password : hash, isAdmin: "false" });

                    //On sauvegarde dans mango db
                    _u.save(function (err, _user) {
                        if (err) {
                            res.status(500).json({"text": "Erreur interne"})

                        //Si tout est bon on redirige vers le login
                        } else {
                            console.log(_user);
                            
                            //On créer un token
                            let token = generateToken(_user._id, false);

                            //On créer un cookie
						    res.cookie('authToken', token );
                            res.status(200);

                            //On redirige
                            res.redirect('/places');
                            console.log("Nouvelle inscription");
                        }
                    })
                });
            }
        });
    }
}

function signupForm(req, res) {
    res.status(200).render('signup');
}

function login (req, res){

    //On vérifie que la personne a bien rentré le mot de passe ou l'email
    if (!req.body.email || !req.body.password) {
        res.status(400).json({"text": "Mot de passe ou email non remplit"})
    }else{
        let recupEmail = req.body.email;

        //On recherche dans la base de donnée si l'utilisateur existe ou non
        users.findOne({ email: recupEmail }, function (err, user) {
            if(user){
                console.log(user['isAdmin']);
                console.log(user);
                //On compare le mot de passe avec celui de la bdd
                bcrypt.compare(req.body.password, user.password, function(err, result) {
                    if(result == true){
                        //On créer un token
                        if(user['isAdmin'] === true){
                            let token = generateToken(user._id, true);

                            //On créer un cookie
                            res.cookie('authToken', token );
    
                            //On redirige
                            res.redirect('/places');
                            res.status(200)
                            console.log("Un admin vient de se connecter"); 
                        }else if (user['isAdmin'] === false){
                            let token = generateToken(user._id, false);

                            //On créer un cookie
                            res.cookie('authToken', token );
    
                            //On redirige
                            res.redirect('/places');
                            res.status(200)
                            console.log("Un utilisateur vient de se connecter");
                        }
                      
                    }
                });
            }else{
                res.status(400).json({"text": "Il n'y a aucun utilisateur"}); 
            }
            
        })
    }

}

function loginForm (req, res){
    res.render('login');
}
   

function logout (req, res){
        res.cookie('authToken', {expires: Date.now()});
        res.clearCookie('authToken');
        res.redirect('/places');
}

function displayProfil (req, res){
    let iduser = req.params
    users.findOne({ _id: iduser['id'] }, function (err, user) {
    res.render('displayProfil', {user})
    })
}
function modifProfil (req, res){
    iduser = req.params
    userBody = req.body
    console.log(req.body.cpsw);
    console.log(!req.body.psw);
    if (req.body.psw == req.body.cpsw) {
        bcrypt.hash(userBody['psw'], 10, function(err, hash) {
            users.findOneAndUpdate( {_id : iduser['id']} ,{ password : hash}, function (err, user) {
                res.status(200).json({"text": "mot de passe modifié"})
            })
        })
    }else{
        console.log("error");
    }
}


function isAuth (req, res, next){

    //On recupère le cookie
    let token = req.cookies;

        //On verifie le token
		jwt.verify(token.authToken, secret.secret, (err, user) => {
            
			if(user){	
				next();
			}else{
                res.redirect('user/login')
				next();
			}
		});
}

exports.signup = signup;
exports.signupForm = signupForm;
exports.login = login;
exports.loginForm = loginForm;
exports.logout = logout;
exports.isAuth = isAuth;
exports.displayProfil = displayProfil;
exports.modifProfil = modifProfil;
