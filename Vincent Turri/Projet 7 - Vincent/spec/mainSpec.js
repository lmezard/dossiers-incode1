describe("Addition", function() {  
  it("addition simple", function() {     
    expect(calcul(5+8)).toEqual(13);
  });
  it("addition complexe", function() {    
    expect(calcul(5.2+8)).toEqual(13.2);
  });
  it("addition negative", function() {    
    expect(calcul(-8+7)).toEqual(-1);
  });
}); 

describe("soustraction", function() {  
  it("soustraction simple", function() {    
    expect(calcul(2-2)).toEqual(0);
  });
  it("soustraction complexe", function() {    
    expect(calcul(2-1.5)).toEqual(0.5);
  });
  it("soustraction negative", function() {    
    expect(calcul(-8-9)).toEqual(-17);
  });
}); 

describe("Multiplication", function() {  
  it("Multiplication simple", function() {    
    expect(calcul(2*2)).toEqual(4);
  });
  it("Multiplication complexe", function() {    
    expect(calcul(2.5*1)).toEqual(2.5);
  });
  it("Multiplication negative", function() {    
    expect(calcul(-8*-2)).toEqual(16);
  });
  it("Multiplication 0", function() {    
    expect(calcul(8*0)).toEqual(0);
  });
}); 

describe("Divsion", function() {  
  it("Divsion simple", function() {    
    expect(calcul(2/2)).toEqual(1);
  });
  it("Divsion complexe", function() {    
    expect(calcul(2.5/1)).toEqual(2.5);
  });
  it("Divsion negative", function() {    
    expect(calcul(-8/-2)).toEqual(4);
  });
  it("Divsion 0", function() {    
    expect(calcul(8/0)).toEqual(Infinity);
  });
}); 

describe("Suite d'opération", function() {  
  it("+-*/", function() {    
    expect(calcul(3+2*1/2)).toEqual(4);
  });
  it("+-*/ complexe", function() {    
    expect(calcul(3.5-6*1/2)).toEqual(0.5);
  });
  it("+-*/ complexe", function() {    
    expect(calcul(7+6-4)).toEqual(9);
  });
 
}); 


describe("addition test", function() {  
  it("+-*/", function() {    
    expect(addition(2,3)).toEqual(5);
  });
}); 


