document.addEventListener('DOMContentLoaded', function(){
    document.getElementsByTagName("form")[0].addEventListener("submit", function(event){
        //afficher les informations dans la console
        event.preventDefault();
        console.log("Nom : "+document.getElementById("nom").value);
        console.log("Prénom : "+document.getElementById("prenom").value);
        console.log("Téléphone : "+document.getElementById("tel").value);
        console.log("Email : "+document.getElementById("mail").value);
        console.log("Message : "+document.getElementById("msg").value);
        
        //ouvrir la pop up
        document.getElementById("overlay").style.display = "flex";

        //fermer la pop up
        document.getElementsByTagName("button")[0].addEventListener("click", function(event){
            event.preventDefault();
            document.getElementById("overlay").style.display = "none";  
        })
    })
    

})